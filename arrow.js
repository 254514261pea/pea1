function test1() {

    console.log('Hello test01');

}
// change function to = then =>
const test2 = () => console.log('Hello test02'); // note () => return something
// test
function sum(a, b) {

    return a + b;

}

// เปลี่ยน function เป็น =

// เปลี่ยน {} เป็น =>

const sumArrow = (a, b) => a + b;
console.log(sumArrow(1, 2));



const operationArrow = (a, b, c) => ((a + b) / c);

console.log(operationArrow(4, 4, 2));